# Customisable Concentrichron
A fork of @vector_gl's space concentrichron that allows a lot of customisation!

## Features
Well, it's a clock, it measures seconds, minutes, hours, days, months and years!

## Customisation
This fork allows the customisation of all it's features in a very neat way.
The config file is the `Style.css` and it has the configs separated for each "ring".